class Api::V1::PostsController < ApplicationController
  before_action :get_post, except: [:index, :create]

  def index
    @posts = Post.all
    render json: {status: 200, message: "Posts listed successfully", data: @posts}
  end

  def create
    @post = Post.new(post_params)
    if @post.save
      render json: { status: 1, code: 200, message: 'Post created successfully', data: @post  }
    else
      render json: { status: 0, code: 400, message: @post.errors.full_messages, data: nil }
    end
  end

  def show
    render json: {status: 200, message: "Post detail", data: @post}
  end

  def update
    if @post.update_attributes(post_params)
      render json: {status: 200, message: "update Success", data: @post}
    else
      render json: {status: 400, message: "Error"}
    end
  end

  def destroy
    if @post.destroy
      render json: {status: 200, message: "Post deleted successfully"}
    else
      render json: {status: 400, message: "Error"}
    end
  end

  def import
    Post.import(params[:file])
    render json: {status: 200, message: "Post imported successfully", data: Post.all}
  end

  private

  def get_post
    @post = Post.find_by(id: params[:id])
  end

  def post_params
    params.require(:post).permit(:brand_name, :product_name, :product_desc, :email, :photo_url, :username)
  end
end
