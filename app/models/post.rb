class Post < ApplicationRecord
  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|
      Post.create! row.to_hash
    end
  end
end
